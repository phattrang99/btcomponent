import React, { Component } from "react";
import Header from "./header";
import Carousel from "./carousel";
import Content from "./content";
import Footer from "./footer";

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default Home;
